<?php
require_once 'include/init.php';


/** Renders jenever view */
class JeneverView extends TemplateView
{
  
}

// Create and run jenever view
$view = new JeneverView('Jenever', 'jenever');
$view->run();