<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/init.php';

/** Renders and processes CRUD operations for the Signup Model */
class UserView extends TemplateView
{
    protected $template_base_name = 'templates/user/user';

    /** 
     * Run the page, but only for logged in committee members. 
     * Non-admins are only allowed to see a list of their redirects
     */
    public function run_page() {
        if (get_session_type() === 'anonymous')
            throw new HttpException(403, 'You need to be logged in as Cover member or through the link of an earlier purchase');
        
        if (array_key_exists('view', $_GET) && $_GET['view'] === 'logout') {
            $next = SERVER_NAME;
            if (array_key_exists('next', $_GET)) {
                // remove token parameter from the next url, as this would make logout useless
                $url_parts = parse_url($_GET['next']);
                if (isset($url_parts['query']))
                    parse_str($url_parts['query'], $url_query);
                else
                    $url_query = [];
                unset($url_query['token']);
                $url_parts['query'] = http_build_query($url_query);
                $next = _http_build_url($url_parts);
            }

            if (get_session_type() === 'token') {
                end_token_session();
                return $this->redirect($next);
            }
            return $this->redirect(cover_logout_url($next));
        }

        return $this->render_template($this->get_template(), [
            'session_type' => get_session_type(),
            'user' => get_user()
        ]);
    }
}

// Create and run subdomain view
$view = new UserView('user', 'User');
$view->run();
