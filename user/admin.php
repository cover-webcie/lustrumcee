<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/init.php';

/** Renders and processes CRUD operations for the Signup Model */
class UserAdminView extends ModelView
{
    protected $views = ['update', 'list'];
    protected $template_base_name = 'templates/user/admin';

    /** 
     * Run the page, but only for logged in committee members. 
     * Non-admins are only allowed to see a list of their redirects
     */
    public function run_page() {
        if (!cover_session_in_committee(ADMIN_COMMITTEE))
            throw new HttpException(403, 'You need to be LustrumCee to see this page!');

        return parent::run_page();
    }

    /** Returns the default context */
    protected function get_default_context() {
        $context = parent::get_default_context();
        if ($this->_view === 'update')
            $context['object'] =  $this->get_object();
        return $context;
    }
}

// Create and run subdomain view
$view = new UserAdminView('user_admin', 'User admin', get_model('User'), new UserForm('user', true));
$view->run();
