<?php

$session_status = session_status();
if($session_status == PHP_SESSION_NONE)
    session_start();

require_once 'include/init.php';
require_once 'include/form.php';

if (array_key_exists('token', $_GET))
    start_token_session($_GET['token']);


function start_token_session($token) {
    $_SESSION['user_token'] = $token;
}


function end_token_session() {
    unset($_SESSION['user_token']);
}


function get_session_type() {
    if (isset($_SESSION['user_token']))
        return 'token';
    elseif (cover_session_logged_in())
        return 'cover';
    else
        return 'anonymous';
}


function get_user ($fresh=false) {
    static $user = null;

    if (isset($user) && !$fresh)
        return $user;

    $model = get_model('User');
    if (get_session_type() === 'token')
        $user = $model->get_by_id($_SESSION['user_token'], 'url') ?? [];
    elseif (cover_session_logged_in())
        $user = $model->get_by_id(get_cover_session()->id, 'cover_id') ?? [];
    else
        $user = [];

    return $user;
}


class UserForm extends Bootstrap3Form
{
    protected $data_source;
    protected $static_fields;

    public function __construct($name, $admin=false, $data_source='', array $static_fields=[]) {
        $this->data_source = $data_source;
        $this->static_fields = $static_fields;

        $model = get_model('User');

        $type_options = $model::$type_options;
        array_unshift($type_options, ['Pick the most relevant…', ['selected', 'disabled']]);
        // Invitee is an automatic/admin-only option
        if (!$admin)
            unset($type_options['invitee']);

        $fields = [
            'type'        => new SelectField ('What are you?', $type_options),
            'name'        => new StringField ('Name',                                   $admin, ['maxlength' => 255, 'placeholder' => 'Jan de Joker']),
            'address'     => new StringField ('Street name + number',                   $admin, ['maxlength' => 255, 'placeholder' => 'Streetname  1A']),
            'postal_code' => new StringField ('Postal code',                            $admin, ['maxlength' => 255, 'placeholder' => '1234 AB']),
            'city'        => new StringField ('Place of residence',                     $admin, ['maxlength' => 255, 'placeholder' => 'Groningen']),
            'email'       => new EmailField  ('Email',                                  $admin, ['placeholder' => 'name@example.com']),
            'phone'       => new StringField ('Phone number',                           $admin, ['maxlength' => 100, 'placeholder' => '+31 (0) 50 1234567']),
            'iban'        => new StringField ('IBAN',                                   $admin, ['maxlength' => 34, 'placeholder' => 'NL37BANK0123456789']),
            'bic'         => new StringField ('BIC (only for non-Dutch bank accounts)', true,   ['maxlength' => 11, 'placeholder' => 'BANKNL2G']),
            'diet'        => new StringField ('Dietary restrictions',                   true,   ['maxlength' => 255]),
        ];
        parent::__construct($name, $fields);
    }

    protected function _render_field($field, array $attributes=[], array $error_attributes=[], array $parent_attributes=[]) {
        if (in_array($field->get_name(), $this->static_fields))
            $attributes[] = 'readonly';
        return parent::_render_field($field, $attributes, $error_attributes, $parent_attributes);
    }

    public function render_data_source_message() {
        $message = '';
        if ($this->data_source == 'member')
            $message = 'We prefilled the form with some of your information from the Cover website. You can change your information there.';
        elseif ($this->data_source == 'db')
            $message = 'We prefilled the form with your previous order/registration and have hidden some fields for your privacy. You can change your information by contacting the LustrumCee.';
        return $message;
    }

    /** Implement custom validation */
    public function validate() {
        $result = parent::validate();

        if ($this->get_value('type') === 'member'){
            if (!cover_session_logged_in()) {
                $this->get_field('type')->errors[] = 'Please login onto the Cover website first';
                $result = false &&  $result;
            } elseif (isset(get_cover_session()->member_till)) {
                $this->get_field('type')->errors[] = 'According to the Cover website, you are not a member';
                $result = false &&  $result;
            }
        }

        if (!array_key_exists('iban', $this->get_fields()) || !array_key_exists('bic', $this->get_fields()))
            return $result;

        if (!\IsoCodes\Iban::validate($this->get_value('iban'))) {
            $this->get_field('iban')->errors[] = 'Please provide a valid IBAN';      
            $result = false &&  $result;
        }

        $bic = $this->get_value('bic');

        // Validate if BIC is set for non-Dutch IBANs.
        if (substr(strtoupper($this->get_value('iban')), 0, 2) !== 'NL') {
            if (!isset($bic) || empty(trim($bic))){
                $this->get_field('bic')->errors[] = 'BIC is required for non-Dutch bank accounts';
                $result = false &&  $result;
            }
        }

        if (isset($bic) && !empty(trim($bic)) && !\IsoCodes\SwiftBic::validate($bic)) {
            $this->get_field('bic')->errors[] = 'Please provide a valid BIC';      
            $result = false &&  $result;
        }

        return $result;
    }
}


function create_user_form ($name='user') {
    $prefilled_data = [];
    $static_fields = [];
    $data_source = '';

    $model = get_model('User');
    if (!empty(get_user())) {
        $data_source = 'db';
        $prefilled_data = get_user();
        $static_fields = array_keys($prefilled_data);
        if (empty(get_user()['diet']) && ($key = array_search('diet', $static_fields)) !== false) {
            unset($static_fields[$key]);
        }
    } elseif (cover_session_logged_in()) {
        $data_source = 'member';
        $member = get_cover_session();
        $prefilled_data = [
            'name' => $member->voornaam . ( empty($member->tussenvoegsel) ? ' ' : ' ' . $member->tussenvoegsel . ' ' ) . $member->achternaam,
            'address' => $member->adres,
            'postal_code' => $member->postcode,
            'city' => $member->woonplaats,
            'email' => $member->email,
            'phone' => $member->telefoonnummer
        ];

        foreach ($prefilled_data as $key => $value)
            if (!empty($value))
                $static_fields[] = $key;

        if (isset($member->member_till)) {
            $prefilled_data['type'] = 'alumnus';
        } else {
            $prefilled_data['type'] = 'member';
            $static_fields[] = 'type';
        }
    }
    
    $form =  new UserForm($name, false, $data_source, $static_fields);

    $form->populate_fields($prefilled_data);

    if (get_session_type() === 'token' && !empty(get_user())) {
        // Remove personal data for token sessions
        $form->delete_field('address');
        $form->delete_field('postal_code');
        $form->delete_field('city');
        $form->delete_field('email');
        $form->delete_field('phone');
        $form->delete_field('iban');
        $form->delete_field('bic');
    }

    return $form;
}


function process_user_data ($data) {
    if(isset($_SESSION['user_token'])) {
        $user_data = get_user();
        unset($user_data['passe_partout_id']);
        unset($user_data['passe_partout_name']);
        $data = array_merge($user_data, $data);
    }
    elseif (cover_session_logged_in())
        $data['cover_id'] = get_cover_session()->id;
    else {
        $data['url'] = rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
        start_token_session($data['url']);
    }
    return $data;
}
