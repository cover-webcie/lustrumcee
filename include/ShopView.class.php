<?php

$session_status = session_status();
if($session_status == PHP_SESSION_NONE)
    session_start();

require_once 'include/utils.php';
require_once 'include/FormView.class.php';

/**
 * Cart: A PHP session based implementation of a shopping cart
 */
class Cart
{
    protected $name;

    /** Initializes the session cart variable if necessary */
    public function __construct($name='cart') {
        $this->name = $name;
        if (!isset($_SESSION[$this->name]) || !is_array($_SESSION[$this->name]))
            $_SESSION[$this->name] = [];
    }

    /** Adds an item to the cart */
    public function add_item($item) {
        $_SESSION[$this->name][uniqid()] = $item;
    }

    /** Deletes an item from the cart */
    public function delete_item($item_id) {
        unset($_SESSION[$this->name][$item_id]);
    }

    /** Returns all items in the cart */
    public function get_items() {
        return $_SESSION[$this->name];
    }

    /** Returns the total number items in the cart */
    public function count_items() {
        try {
            return array_reduce( $_SESSION[$this->name], function($carry, $item) {
                if (isset($item['amount']) && is_numeric($item['amount']))
                    return $carry + $item['amount'];
                return $carry + 1;
            }, 0);
        } catch (Exception $e) {
            return count($_SESSION[$this->name]);
        }
    }

    /** Removes all items from the cart */
    public function empty() {
        $_SESSION[$this->name] = [];
    }
}


/**
 * ModelView: A class to manage CRUD(L) actions for a Model object.
 * Separates reading a single object and listing multiple objects for clarity.
 */
abstract class ShopView extends FormView
{
    // The names of the available views (override to limit actions)
    protected $views = ['list', 'add', 'delete', 'purchase', 'clear'];

    // The default view to run if $_GET['view'] is not provided
    protected $default_view = 'list';

    protected $_view;
    protected $_cart;
    protected $_object;


    public function __construct($page_id, $title='') {
        parent::__construct($page_id, $title);

        if (!isset($_GET['view']))
            $this->_view = $this->default_view;
        else
            $this->_view = $_GET['view'];
    }

    /** Runs the correct function based on the $_GET['view'] parameter */
    protected function run_page() {
        if (!in_array($this->_view, $this->views))
            throw new HttpException(404, 'View not found!');

        if ($this->_view === 'list')
            return $this->run_list();
        elseif ($this->_view === 'add')
            return $this->run_add();
        elseif ($this->_view === 'delete')
            return $this->run_delete();
        elseif ($this->_view === 'clear')
            return $this->run_clear();
        elseif ($this->_view === 'purchase')
            return $this->run_purchase();
        else
            throw new HttpException(404, 'View not found!');
    }

    /** Runs the list_products view */
    protected function run_list() {
        return $this->render_template($this->get_template(), ['objects' => $this->get_model('product')->get()]);
    }

    /** Runs the add view */
    protected function run_add() {
        $product = $this->get_object('product');

        $form = $this->get_product_form($product);
        return $this->run_form($form);
    }

    /** Runs the delete view */
    protected function run_delete() {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST')
            throw new HttpException(405, 'Method not allowed!');

        if (!isset($_GET['id']))
            throw new HttpException(400, 'Please provide an ID!');

        $this->process_delete($_GET['id']);
        $this->redirect($this->get_success_url('purchase'));
    }

    /** Runs the clear view */
    protected function run_clear() {
        $this->get_cart()->empty();
        $this->redirect($this->get_success_url());
    }

    /** Runs the purchase view */
    protected function run_purchase() {
        $form = $this->get_user_form();
        return $this->run_form($form);
    }

    /** Processes a valid form according to the view */
    protected function form_valid($form){
        if ($this->_view === 'purchase') {
            try {
                $this->process_form_data($form->get_values());
                $context = ['status' =>  'success'];
            } catch (Exception $e) {
                $context = [
                    'status' => 'error', 
                    'message' => $e->getMessage()
                ];
            }
            return $this->render_template($this->get_template('purchase_processed'), $context);
        }
        return parent::form_valid($form);
    }

    protected function form_invalid($form){
        if ($this->_view === 'add')
            $template = $this->get_template('product_form');
        elseif ($this->_view === 'purchase')
            $template = $this->get_template('purchase_form');
        else
            throw new RuntimeException('Incompatible view while rendering invalid form!');
        return $this->render_template($template, ['form' => $form]);
    }

    /** Processes the form data for create and update */
    protected function process_form_data($data) {
        if ($this->_view === 'add')
            $this->process_add($data);
        elseif ($this->_view === 'purchase')
            $this->process_purchase($data);
        else
            throw new RuntimeException('Incompatible view while processing form!');
    }

    /**
     * Processes the form data for the add view:
     * Adds an item to the cart
     */
    protected function process_add($data) {
        $data['product'] = $this->get_object('product');
        $this->get_cart()->add_item($data);
    }

    /** 
     * Processes the form data for the delete view:
     * Deletes an item from the cart.
     */
    protected function process_delete($item_id) {
        $this->get_cart()->delete_item($item_id);    
    }

    /**
     * Processes the form data for the purchase view:
     * Creates a user, creates purchases and empties the cart.
     */
    protected function process_purchase($data) {
        $user_id = $this->get_model('user')->create($data);

        foreach ($this->get_cart()->get_items() as $item) {
            $item['user_id'] = $user_id;
            $this->get_model('purchase')->create($item);
        }
            
        $this->get_cart()->empty();    
    }

    /** Returns the Cart object tot use for the view */
    protected function get_cart(){
        if (!isset($this->_cart))
            $this->_cart = new Cart();
        return $this->_cart;
    }

    /** Returns the default context */
    protected function get_default_context() {
        $context = parent::get_default_context();
        if ($this->_view === 'add')
            $context['object'] = $this->get_object('product');
        $context['cart'] = $this->get_cart();
        return $context;
    }

    /**
     * Returns the object of the correct model type referenced to by the 
     * $_GET['id'] parameter 
     */
    protected function get_object($type='product') {
        if (!isset($_GET['id']))
            throw new HttpException(400, 'Please provide an ID!');

        if (!isset($this->_object)){
            $this->_object = $this->get_model($type)->get_by_id($_GET['id']);

            if (empty($this->_object))
                throw new HttpException(404, 'No object found for id');
        }

        return $this->_object;
    }

    /** Returns the url to redirect to after (successful) create, update or delete */
    protected function get_success_url($view=null) {
        if (!isset($view))
            $view = $this->default_view;
        $parts = explode('?', $_SERVER['REQUEST_URI'], 2);
        return sprintf('%s?view=%s', $parts[0], $view);
    }

    /** Returns the name of the template to use to render the current view */
    protected function get_template($view_name='') {
        if (empty($view_name))
            $view_name = $this->_view;

        return parent::get_template($view_name);
    }

    /** Returns the Model object to use for the view */
    abstract protected function get_model($type='product');

    /** Returns the Form to use to add a product to the cart */
    abstract protected function get_product_form($product_id);

    /** Returns the Form to use to obtain user information */
    abstract protected function get_user_form();
}
