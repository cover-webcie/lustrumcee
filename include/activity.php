<?php
require_once 'include/sessions.php';

function get_cover_activities() {
    static $activities = null;

    if ($activities !== null)
        return $activities;

    $activities = [];

    foreach (cover_get_json('agenda') as $activity )
        if ($activity->committee__login == ADMIN_COMMITTEE){
            $activity->beschrijving = str_replace(CALENDAR_MESSAGE, '', $activity->beschrijving);
            $activities[$activity->id] = $activity;
        }

    return $activities;
}

function inject_activity_data($activities, $field='cover_id') {
    if (array_key_exists($field, $activities))
        $activities['details'] =  get_cover_activities()[$activities[$field]];
    else {
        foreach ($activities as $id => &$activity) {
            if (array_key_exists($activity[$field], get_cover_activities()))
                $activity['details'] = get_cover_activities()[$activity[$field]];
            else
                unset($activities[$id]);
        }
    }
    return $activities;
}
