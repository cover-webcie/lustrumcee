<?php
require_once 'include/models/Model.class.php';

class Registration extends Model
{
    protected $get_view = 'v_registration';

    public function __construct($db) {
        parent::__construct($db, 'registration');
    }
}