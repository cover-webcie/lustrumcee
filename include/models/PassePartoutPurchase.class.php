<?php
require_once 'include/models/Model.class.php';

class PassePartoutPurchase extends Model
{
    protected $get_view = 'v_passe_partout_purchase';

    public function __construct($db) {
        parent::__construct($db, 'passe_partout_purchase');
    }
}
