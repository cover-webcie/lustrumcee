<?php
require_once 'include/models/Model.class.php';

class User extends Model
{
    protected $get_view = 'v_user';

    public static $type_options = [
        'member' => 'Cover member',
        'alumnus' => 'Former Cover member',
        'staff' => 'FSE Staff',
        'invitee' => 'Invitee',
        'external' => 'Other',
    ];

    public function __construct($db) {
        parent::__construct($db, 'user');
    }
}
