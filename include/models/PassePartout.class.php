<?php
require_once 'include/models/Model.class.php';

class PassePartout extends Model
{
    protected $get_view = 'v_passe_partout';

    public function __construct($db) {
        parent::__construct($db, 'passe_partout');
    }
}
