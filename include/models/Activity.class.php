<?php
require_once 'include/models/Model.class.php';
require_once 'include/activity.php';

class Activity extends Model
{
    protected $get_view = 'v_activity';

    public function __construct($db) {
        parent::__construct($db, 'activity');
    }

    public function get(array $conditions=[], array $order=[], $get_first=false) {
        return inject_activity_data(parent::get($conditions, $order, $get_first));
    }
}
