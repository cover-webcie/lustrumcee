ALTER TABLE user ADD COLUMN diet VARCHAR(255);

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cover_id` int NOT NULL,
  `allowed_types` varchar(255) NOT NULL DEFAULT 'member',
  `price` decimal(6,2) NOT NULL,
  `available_amount` int,
  `deadline` DATETIME,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `needs_registration` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE (`cover_id`)
) ENGINE = INNODB;

--
-- Table structure for table `registration`
--

DROP TABLE IF EXISTS `registration`;
CREATE TABLE `registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `timestamp` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `accept_terms` tinyint(1) NOT NULL DEFAULT 0,
  `accept_costs` tinyint(1) NOT NULL DEFAULT 0,
  `remarks` varchar(2048),
  PRIMARY KEY (`id`),
  FOREIGN KEY fk_registration_user (user_id)
    REFERENCES user (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  FOREIGN KEY fk_registration_activity (activity_id)
    REFERENCES activity (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = INNODB;


--
-- View definition for table `v_registration_totals` (helper view for `v_activity`)
--

DROP VIEW IF EXISTS `v_registration_totals`;
CREATE VIEW `v_registration_totals` AS
  SELECT registration.activity_id activity_id
        ,count(registration.id) amount
    FROM registration
   WHERE status = 1
   GROUP BY registration.activity_id;


--
-- View definition for table `v_activity`
--

DROP VIEW IF EXISTS `v_activity`;
CREATE VIEW `v_activity` AS
  SELECT activity.*
        ,COALESCE(registrations.amount, 0) registrations
    FROM activity 
         LEFT JOIN v_registration_totals registrations
           ON activity.id = registrations.activity_id;


--
-- View definition for table `v_registration`
--

DROP VIEW IF EXISTS `v_registration`;
CREATE VIEW `v_registration` AS
  SELECT registration.*
        ,user.name user_name
        ,activity.cover_id activity_cover_id
        ,activity.deadline activity_deadline
    FROM registration
         LEFT JOIN activity
           ON registration.activity_id = activity.id
         LEFT JOIN user
           ON registration.user_id = user.id;
