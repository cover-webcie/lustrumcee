--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cover_id` int,
  `type` enum('member','external','alumnus','invitee','staff') NOT NULL,
  `name` varchar(255),
  `address` varchar(255),
  `postal_code` varchar(255),
  `city` varchar(255),
  `email` varchar(254), -- official max length of an email-address
  `phone` varchar(100),
  `iban` varchar(34), -- official max length of an iban
  `bic` varchar(11), -- official max length of a bic
  `url` varchar(100),
  PRIMARY KEY (`id`),
  UNIQUE (`cover_id`),
  UNIQUE (`url`)
) ENGINE = INNODB;


--
-- Table structure for table `shop_product`
--

DROP TABLE IF EXISTS `shop_product`;
CREATE TABLE `shop_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(2048),
  `price` decimal(6,2) NOT NULL,
  `sizes` varchar(255),
  `colours` varchar(255),
  `available_amount` int,
  `deadline` DATETIME,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE (`name`)
) ENGINE = INNODB;


--
-- Table structure for table `shop_purchase`
--

DROP TABLE IF EXISTS `shop_purchase`;
CREATE TABLE `shop_purchase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `timestamp` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `amount` int NOT NULL DEFAULT 1,
  `size` varchar(255),
  `colour` varchar(255),
  `accept_terms` tinyint(1) NOT NULL DEFAULT 0,
  `accept_costs` tinyint(1) NOT NULL DEFAULT 0,
  `remarks` varchar(2048),
  PRIMARY KEY (`id`),
  FOREIGN KEY fk_shop_purchase_user (user_id)
    REFERENCES user (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  FOREIGN KEY fk_shop_purchase_product (product_id)
    REFERENCES shop_product (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = INNODB;


--
-- View definition for table `v_shop_purchase_totals` (helper view for `v_shop_product`)
--

DROP VIEW IF EXISTS `v_shop_purchase_totals`;
CREATE VIEW `v_shop_purchase_totals` AS
  SELECT purchase.product_id product_id
        ,sum(purchase.amount) amount
    FROM shop_purchase purchase
   WHERE status = 1
   GROUP BY purchase.product_id;


--
-- View definition for table `v_shop_product`
--

DROP VIEW IF EXISTS `v_shop_product`;
CREATE VIEW `v_shop_product` AS
  SELECT product.*
        ,COALESCE(orders.amount, 0) orders
    FROM shop_product product 
         LEFT JOIN v_shop_purchase_totals orders
           ON product.id = orders.product_id;


--
-- View definition for table `v_shop_purchase`
--

DROP VIEW IF EXISTS `v_shop_purchase`;
CREATE VIEW `v_shop_purchase` AS
  SELECT purchase.*
        ,user.name user_name
        ,product.name product_name
        ,product.deadline product_deadline
    FROM shop_purchase purchase
         LEFT JOIN shop_product product
           ON purchase.product_id = product.id
         LEFT JOIN user
           ON purchase.user_id = user.id;
