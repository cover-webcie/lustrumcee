--
-- Table structure for table `passe_partout`
--

DROP TABLE IF EXISTS `passe_partout`;
CREATE TABLE `passe_partout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT 'member',
  `description` varchar(1024),
  `allowed_types` varchar(255) NOT NULL DEFAULT 'member',
  `price` decimal(6,2) NOT NULL,
  `available_amount` int,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE (`name`)
) ENGINE = INNODB;

--
-- Table structure for table `passe_partout_activity`
--

DROP TABLE IF EXISTS `passe_partout_activity`;
CREATE TABLE `passe_partout_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `passe_partout_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY fk_passe_partout_activity_passe_partout (passe_partout_id)
    REFERENCES passe_partout (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  FOREIGN KEY fk_passe_partout_activity_activity (activity_id)
    REFERENCES activity (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = INNODB;


--
-- Table structure for table `passe_partout_purchase`
--

DROP TABLE IF EXISTS `passe_partout_purchase`;
CREATE TABLE `passe_partout_purchase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `passe_partout_id` int(11) NOT NULL,
  `timestamp` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `accept_terms` tinyint(1) NOT NULL DEFAULT 0,
  `accept_costs` tinyint(1) NOT NULL DEFAULT 0,
  `remarks` varchar(2048),
  PRIMARY KEY (`id`),
  UNIQUE (`user_id`),
  FOREIGN KEY fk_passe_partout_purchase_user (user_id)
    REFERENCES user (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  FOREIGN KEY fk_passe_partout_purchase_passe_partout (passe_partout_id)
    REFERENCES passe_partout (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = INNODB;


--
-- View definition for table `v_passe_partout_totals` (helper view for `v_activity`)
--

DROP VIEW IF EXISTS `v_passe_partout_totals`;
CREATE VIEW `v_passe_partout_totals` AS
  SELECT passe_partout_purchase.passe_partout_id passe_partout_id
        ,count(passe_partout_purchase.id) amount
    FROM passe_partout_purchase
   GROUP BY passe_partout_purchase.passe_partout_id;


--
-- View definition for table `v_passe_partout`
--

DROP VIEW IF EXISTS `v_passe_partout`;
CREATE VIEW `v_passe_partout` AS
  SELECT passe_partout.*
        ,COALESCE(registrations.amount, 0) registrations
    FROM passe_partout 
         LEFT JOIN v_passe_partout_totals registrations
           ON passe_partout.id = registrations.passe_partout_id;


--
-- View definition for table `v_user`
--

DROP VIEW IF EXISTS `v_user`;
CREATE VIEW `v_user` AS
  SELECT user.*
        ,passe_partout.id passe_partout_id
        ,passe_partout.name passe_partout_name
    FROM user
         LEFT JOIN passe_partout_purchase purchase
           ON user.id = purchase.user_id
         LEFT JOIN passe_partout
           ON purchase.passe_partout_id = passe_partout.id;


--
-- View definition for table `v_passe_partout_purchase`
--

DROP VIEW IF EXISTS `v_passe_partout_purchase`;
CREATE VIEW `v_passe_partout_purchase` AS
  SELECT passe_partout_purchase.*
        ,user.name user_name
        ,passe_partout.name passe_partout_name
    FROM passe_partout_purchase
         LEFT JOIN passe_partout
           ON passe_partout_purchase.passe_partout_id = passe_partout.id
         LEFT JOIN user
           ON passe_partout_purchase.user_id = user.id;


--
-- View definition for table `v_passe_partout_activity`
--

DROP VIEW IF EXISTS `v_passe_partout_activity`;
CREATE VIEW `v_passe_partout_activity` AS
  SELECT passe_partout_activity.*
        ,activity.cover_id activity_cover_id
    FROM passe_partout_activity
         LEFT JOIN activity
           ON passe_partout_activity.activity_id = activity.id;
