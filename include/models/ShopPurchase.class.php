<?php
require_once 'include/models/Model.class.php';

class ShopPurchase extends Model
{
    const STATUS_CANCELLED = -1;
    const STATUS_PENDING = 0;
    const STATUS_CONFIRMED = 1;
    const STATUS_OPTIONS = [
        'PENDING' => self::STATUS_PENDING,
        'CANCELLED' => self::STATUS_CANCELLED,
        'CONFIRMED' => self::STATUS_CONFIRMED
    ];

    protected $get_view = 'v_shop_purchase';

    public function __construct($db) {
        parent::__construct($db, 'shop_purchase');
    }
}
