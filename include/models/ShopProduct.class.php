<?php
require_once 'include/models/Model.class.php';

class ShopProduct extends Model
{
    protected $get_view = 'v_shop_product';

    public function __construct($db) {
        parent::__construct($db, 'shop_product');
    }
}
