<?php

function deadline_has_lapsed($object) {
    return !empty($object['deadline']) && (new DateTime()) > date_create_from_format('Y-m-d H:i:s', $object['deadline']);
}

function is_sold_out($object, $amount=1, $field='order') {
    return is_numeric($object['available_amount']) && $object['available_amount'] - $object[$field] < $amount;
}
