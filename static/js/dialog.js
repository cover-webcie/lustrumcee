$(document).on('click', 'a[data-popup], [data-popup][type=submit]', function(e) {
    if (e.shiftKey || e.ctrlKey || e.metaKey)
        return;

    e.preventDefault();

    var $link = $(this);
    var $placeholder = $('<div class="modal">').appendTo(document.body);

    if($link.find('i.fa').length){
        var $icon = $link.find('i.fa').first();
        $icon.data('icon', $icon.attr('class'));
        $icon.removeClass();
        $icon.addClass('fa fa-circle-o-notch fa-spin fa-fw');
    } else {
        $link.button('loading');
    }

    function createModal($body, title) {
        $placeholder.html('\
            <div class="modal-dialog modal-lg image-modal">\
                <div class="modal-content">\
                    <div class="modal-header">\
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
                        <h4 class="modal-title"></h4>\
                    </div>\
                    <div class="modal-body">\
                    </div>\
                </div>\
            </div>');
        $placeholder.find('.modal-body').append($body);
        $placeholder.find('.modal-title').text(title);    
    }

    switch ($link.data('popup')){
        case 'modal':
            if($link.prop('type') === 'submit') {
                var $form = $link.closest('form');
                console.log($form.serializeArray());
                var data =$form.serializeArray();
                console.log(data);
                var url = $form.attr('action');
            } else {
                var data = null;
                var url = $link.prop('href');
            }
            $placeholder.load(url + ' .modal-dialog', data, function(response, status, xhr) {
                if (status === 'error') {
                    var body = $(response).filter('#error').html();
                    if (!body)
                        body = response;
                    createModal(body, 'Something unexpected happened: ' + xhr.status + ' ' + xhr.statusText);
                }
                $placeholder.modal('show');
            });
            break;

        case 'image':
            createModal(
                $('<img>').prop('src', $link.prop('href')).css('max-width', '100%'),
                $link.prop('title')
            );
            $placeholder.modal('show');
            break;

        default:
            throw Error("Unknown type '" + $link.data('popup') + "'");
    }

    $placeholder.on('click', '*[data-dismiss=modal]', function(e) {
        e.preventDefault();
        $placeholder.modal('hide');
    });

    $placeholder.on('shown.bs.modal', function() {
        $placeholder.trigger('partial-ready');
    });

    // Remove dialog from HTML when hidden
    $placeholder.on('hidden.bs.modal', function() {
        if($link.find('i.fa').length){
            var $icon = $link.find('i.fa').first();
            $icon.removeClass();
            $icon.addClass($icon.data('icon'));
        }else{
            $link.button('reset');
        }
        $placeholder.remove();
    });
});
