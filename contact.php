<?php
require_once 'include/init.php';


/** Renders and processes the request form */
class ContactView extends FormView
{
    protected $template_base_name = 'templates/contact/contact';
    /** Creates and returns the request form */
    protected function get_form() {
        // Create needed for options
        $subject_options = [
            ['What is your question about?', ['disabled', 'selected']],
            'I\'ve got an idea!',
            'Activities',
            'Merchandise',
            'My information',
            'Other'
        ];

        // Create fields
        $fields = [
            'name'       => new StringField ('Name', false, ['maxlength' => 255]),
            'email'      => new EmailField  ('Email', false, ['maxlength' => 255]),
            'subject'    => new SelectField ('Subject', $subject_options),
            'question'   => new TextAreaField ('Your question', false, ['maxlength' => 4096]),
        ];

        return new Bootstrap3Form('request', $fields);
    }

    /** Renders response indicating whether the valid form was successfully processed (or not) */
    protected function form_valid($form){
        try {
            $this->process_form_data($form->get_values());
            $context = ['status' =>  'success'];
        } catch (Exception $e) {
            $context = [
                'status' => 'error', 
                'message' => 'Something went wrong: ' . $e->getMessage()
            ];
        }
        return $this->render_template($this->get_template('form_processed'), $context);
    }
    
    /** Processes the data of a valid form */
    protected function process_form_data($data) {
        $email = sprintf('%s <%s>', filter_var($data['name'], FILTER_SANITIZE_EMAIL), filter_var($data['email'], FILTER_SANITIZE_EMAIL));
        $success = send_mail(
            'no-reply@svcover.nl',
            ADMIN_EMAIL,
            $this->render_template($this->get_template('email'), $data),
            null,
            [ sprintf('Reply-To: %s', $email), sprintf('Bcc: %s', $email) ]
        );

        // Determine wether email has ben send succesfully
        if (!$success)
            throw new HttpException(500, 'We failed to send submit your question for some reason.');
    }
}

// Create and run domain request view
$view = new ContactView('contact', 'Contact');
$view->run();
