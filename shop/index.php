<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/ShopView.class.php';
require_once 'include/init.php';


/** Renders and processes CRUD operations for the Signup Model */
class MerchandiseShopView extends ShopView
{
    protected $template_base_name = 'templates/shop/shop';

    /** Returns the Model object to use for the view */
    protected function get_model($type='product') {
        if ($type === 'product')
            return get_model('ShopProduct');
        elseif ($type === 'purchase')
            return get_model('ShopPurchase');
        elseif ($type === 'user')
            return get_model('User');
        else
            throw new RuntimeException('Unknown model requested!');
    }

    /** Returns the Form to use to add a product to the cart */
    protected function get_product_form($product) {
        $fields = [ 'amount' => new NumberField ('Amount', false, ['min' => 1]) ];

        if (!empty($product['sizes'])) {
            $size_options = array_map('trim', explode(';', $product['sizes']));
            array_unshift($size_options, ['Pick your size…', ['selected', 'disabled']]);
            $fields['size'] = new SelectField ('Size', $size_options);
        }

        if (!empty($product['colours'])) {
            $colour_options = array_map('trim', explode(';', $product['colours']));
            array_unshift($colour_options, ['Pick your colour…', ['selected', 'disabled']]);
            $fields['colour'] = new SelectField ('Colour', $colour_options);
        }

        $form_name = preg_replace('/[^A-Za-z0-9-]+/', '-', $product['name']);

        $form = new Bootstrap3Form($form_name . '-form', $fields);
        $form->populate_field('amount', 1);
        return $form;
    }

    /** Returns the Form to use to obtain user information */
    protected function get_user_form() {
        $form = create_user_form();
        $form->delete_field('diet');
        $form->add_fields([
            'remarks' => new TextAreaField ('Remarks', true),
            'accept_terms' => new CheckBoxField ('I have read and accepted the terms and conditions', false),
            'accept_costs' => new CheckBoxField ('I allow Cover to deduct the costs from my bank account through SEPA direct debit', false),
        ]);
        return $form;
    }

    /**
     * Processes the form data for the purchase view:
     * Creates a user, creates purchases and empties the cart.
     */
    protected function process_purchase($data) {
        $data = process_user_data($data);

        $user_data = $data;
        unset($user_data['accept_terms']);
        unset($user_data['accept_costs']);
        unset($user_data['remarks']);

        if (empty(get_user()))
            $user_id = $this->get_model('user')->create($user_data);
        else
            $user_id = get_user()['id'];

        $products = $this->get_model('product')->get();
        $errors = [];

        foreach ($this->get_cart()->get_items() as $id => $item) {
            $item['user_id'] = $user_id;
            $item['accept_terms'] = empty($data['accept_terms']) ? 0 : 1;
            $item['accept_costs'] = empty($data['accept_costs']) ? 0 : 1;
            $item['remarks'] = $data['remarks'];
            $item['product_id'] = $item['product']['id'];

            $product = array_filter($products, function($val) use ($item) {
                return $val['id'] == $item['product_id']; 
            });
            $product = array_shift($product);

            if (!empty($product['deadline']) && (new DateTime()) > date_create_from_format('Y-m-d H:i:s', $product['deadline'])) {
                $errors[] = sprintf('The order deadline for %s has lapsed.', $product['name']);
                $this->get_cart()->delete_item($id);
                continue;
            }

            if (is_numeric($product['available_amount']) && $product['available_amount'] - $product['orders'] < $item['amount']) {
                $errors[] = sprintf('We\'re out of stock for: %s × %s.', $item['amount'], $product['name']);
                $this->get_cart()->delete_item($id);
                continue;
            }

            unset($item['product']);
            $this->get_model('purchase')->create($item);
        }

        $data['type'] = $this->get_model('user')::$type_options[$data['type']];
        $data['cart'] = $this->get_cart()->get_items();
        $this->get_cart()->empty();    

        $email = sprintf('%s <%s>', filter_var($data['name'], FILTER_SANITIZE_EMAIL), filter_var($data['email'], FILTER_SANITIZE_EMAIL));
        $success = send_mail(
            'no-reply@svcover.nl',
            $email,
            $this->render_template($this->get_template('email'), $data),
            null,
            [ sprintf('Reply-To: %s', ADMIN_EMAIL) ]
        );

        // Determine wether email has ben send succesfully
        if (!$success)
            $errors[] = 'Your purchase was stored in our database, but we failed to send a confirmation email.';

        if (!empty($errors))
            throw new HttpException(500, implode("\n\n", $errors));
    }
}

// Create and run subdomain view
$view = new MerchandiseShopView('shop', 'Shop');
$view->run();
