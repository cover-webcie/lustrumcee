<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/init.php';

/** Renders and processes CRUD operations for the Signup Model */
class ShopOrdersView extends ModelView
{
    protected $views = ['update', 'list'];
    protected $template_base_name = 'templates/shop/orders';

    /** 
     * Run the page, but only for logged in committee members. 
     * Non-admins are only allowed to see a list of their redirects
     */
    public function run_page() {
        if (isset($_GET['mode']) && $_GET['mode']  === 'admin' && !cover_session_in_committee(ADMIN_COMMITTEE))
            throw new HttpException(403, 'You need to be LustrumCee to see this page as admin!');
            
        if ($this->_view === 'update' && !cover_session_in_committee(ADMIN_COMMITTEE))
            throw new HttpException(403, 'You need to be LustrumCee to see this page!');
        
        // if ($this->_view === 'cancel')
        //     return $this->run_cancel();

        return parent::run_page();
    }

    /** Run the list view and make admins and non-admins see a different set of subdomains */
    protected function run_list() {
        if (isset($_GET['mode']) && $_GET['mode']  === 'admin' && cover_session_in_committee(ADMIN_COMMITTEE))
            $objects = $this->get_model()->get([], ['id']);
        else
            $objects = $this->get_model()->get(['user_id' => get_user()['id']], ['id']);

        return $this->render_template($this->get_template(), ['objects' => $objects]);
    }

    /** Runs the delete view */
    protected function run_cancel() {
        $object = $this->get_object();

        if ($_SERVER['REQUEST_METHOD'] !== 'POST')
            return $this->render_template($this->get_template(), ['object' => $object]);

        if (!cover_session_in_committee(ADMIN_COMMITTEE) && (new DateTime()) > date_create_from_format('Y-m-d H:i:s', $object['product_deadline']))
            throw new HttpException(400, 'You cannot cancel after the deadline!');

        $this->get_model()->update_by_id($object['id'], ['status' => 0]);
        $this->redirect($this->get_success_url());
    }

    /** Create and returns the form to use for create and update */
    protected function get_form() {
        $object = $this->get_object();

        // Create fields
        $fields = [
            'amount' => new NumberField   ('Amount', false,  ['min' => 1]),
            'size'   => new StringField   ('Sizes',  true,   ['maxlength' => 255]),
            'colour' => new StringField   ('Colour', true,   ['maxlength' => 255]),
            'remarks' => new TextAreaField ('Remarks', true),
            'status' => new CheckBoxField ('Active', true),
        ];

        return new Bootstrap3Form('purchase', $fields);
    }

    /** Maps a valid form to its database representation */
    protected function process_form_data($data) {
        $data['status'] = empty($data['status']) ? 0 : 1;        
        parent::process_form_data($data);   
    }

    /** Returns the default context */
    protected function get_default_context() {
        $context = parent::get_default_context();

        $context['mode'] = 'user';
        if (isset($_GET['mode']))
            $context['mode'] = $_GET['mode'];

        if ($this->_view === 'update')
            $context['object'] =  $this->get_object();
        
        return $context;
    }
}

// Create and run subdomain view
$view = new ShopOrdersView('shop_orders', 'Shop orders', get_model('ShopPurchase'));
$view->run();