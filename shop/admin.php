<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/init.php';


class ProductForm extends Bootstrap3Form
{
    public function __construct() {
        // Create fields
        $fields = [
            'name'             => new StringField   ('Name',                         false, ['maxlength' => 255, 'placeholder' => 'Hello World T-Shirt']),
            'description'      => new TextAreaField ('Description',                  true,  ['maxlength' => 2048]),
            'price'            => new NumberField   ('Price',                        false, ['min' => 0, 'max' => 9999.99, 'step' => 0.01, 'placeholder' => '15.99']),
            'sizes'            => new StringField   ('Available sizes (optional)',   true,  ['maxlength' => 255, 'placeholder' => 'S;M;L;XL']),
            'colours'          => new StringField   ('Available colours (optional)', true,  ['maxlength' => 255, 'placeholder' => 'Red;Blue;Black']),
            'available_amount' => new NumberField   ('Available amount (optional)',  true,  ['min' => 0, 'placeholder' => 'leave blank for unlimited']),
            'deadline'         => new StringField   ('Order deadline (optional)',    true,  ['placeholder' => '2018-09-20 00:00']),
            'active'           => new CheckBoxField ('Active',                       true)
        ];

        parent::__construct('product', $fields);
    }

    /** Implement custom validation */
    public function validate() {
        $result = parent::validate();

        $deadline = $this->get_value('deadline');
        if (isset($deadline) && !empty(trim($deadline)) && date_parse_from_format('Y-m-d H:i', $deadline)['error_count'] !== 0) {   
            $this->get_field('deadline')->errors[] = 'Deadline should be of format YYYY-MM-DD HH:MM';
            $result = false &&  $result;
        }

        return $result;
    }
}


/** Renders and processes CRUD operations for the ShopProduct Model */
class ShopAdminView extends ModelView
{
    protected $views = ['create', 'update', 'list'];
    protected $template_base_name = 'templates/shop/admin';

    /** 
     * Run the page, but only for logged in committee members. 
     * Non-admins are only allowed to see a list of their redirects
     */
    public function run_page() {
        if (!cover_session_logged_in())
            throw new HttpException(401, 'Unauthorized', sprintf('<a href="%s" class="btn btn-primary">Login and get started!</a>', cover_login_url()));
        else if (!cover_session_in_committee(ADMIN_COMMITTEE))
            throw new HttpException(403, 'You need to be LustrumCee to see this page!');
        else
            return parent::run_page();
    }

    /** Maps a valid form to its database representation */
    protected function process_form_data($data) {
        if (empty($data['deadline']))
            $data['deadline'] = null;

        if (!is_numeric($data['available_amount']))
            $data['available_amount'] = null;

        // Convert booleans to tinyints
        $data['active'] = empty($data['active']) ? 0 : 1;
        
        parent::process_form_data($data);   
    }
}

// Create and run subdomain view
$view = new ShopAdminView('shop_admin', 'Shop Admin', get_model('ShopProduct'), new ProductForm());
$view->run();
