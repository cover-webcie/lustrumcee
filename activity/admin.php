<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/init.php';



class ActivityForm extends Bootstrap3Form
{
    public function __construct() {
        // Fetch agenda data from Cover API
        $added_activities = array_map(
            function ($activity) {
                return $activity['cover_id'];
            },
            get_model('Activity')->get()
        );

        $agenda = array_filter(
            get_cover_activities(),
            function ($activity) use ($added_activities) {
                return !in_array($activity->id, $added_activities);
            }
        );

        $activity_options = [['Please choose an activity', ['disabled', 'selected']]];
        // Create options for activities in the agenda and backup to global agenda for convenience
        foreach ($agenda as $activity)
            $activity_options['_' . $activity->id] = sprintf('%s – %s', $activity->kop, date_format(date_create($activity->van), 'Y-m-d H:i'));

        // Create fields
        $fields = [
            'cover_id'           => new SelectField   ('Activity', $activity_options),
            'allowed_types'      => new StringField   ('Allowed participant types',        false,  ['maxlength' => 255, 'placeholder' => implode(';', array_keys(get_model('User')::$type_options))]),
            'price'              => new NumberField   ('Price',                            false, ['min' => 0, 'max' => 9999.99, 'step' => 0.01, 'placeholder' => '15.99']),
            'available_amount'   => new NumberField   ('Available spots (optional)',       true,  ['min' => 0, 'placeholder' => 'leave blank for unlimited']),
            'deadline'           => new StringField   ('Registration deadline (optional)', true,  ['placeholder' => '2018-09-20 00:00']),
            'active'             => new CheckBoxField ('Active',                           true),
            'needs_registration' => new CheckBoxField ('Registration',                     true)
        ];

        parent::__construct('product', $fields);
    }

    /** Implement custom validation */
    public function validate() {
        $result = parent::validate();

        $deadline = $this->get_value('deadline');
        if (isset($deadline) && !empty(trim($deadline)) && date_parse_from_format('Y-m-d H:i', $deadline)['error_count'] !== 0) {   
            $this->get_field('deadline')->errors[] = 'Deadline should be of format YYYY-MM-DD HH:MM';
            $result = false &&  $result;
        }

        if (!empty(array_diff(explode(';', $this->get_value('allowed_types')), array_keys(get_model('User')::$type_options)))) {
            $this->get_field('allowed_types')->errors[] = 'Allowed participant types must be one or more of the following options: ' . implode(';', array_keys(get_model('User')::$type_options));
            $result = false &&  $result;
        }

        return $result;
    }
}


/** Renders and processes CRUD operations for the ShopProduct Model */
class ActivityAdminView extends ModelView
{
    protected $views = ['create', 'update', 'list'];
    protected $template_base_name = 'templates/activity/admin';

    /** 
     * Run the page, but only for logged in committee members. 
     * Non-admins are only allowed to see a list of their redirects
     */
    public function run_page() {
        if (!cover_session_logged_in())
            throw new HttpException(401, 'Unauthorized', sprintf('<a href="%s" class="btn btn-primary">Login and get started!</a>', cover_login_url()));
        else if (!cover_session_in_committee(ADMIN_COMMITTEE))
            throw new HttpException(403, 'You need to be LustrumCee to see this page!');
        else
            return parent::run_page();
    }

    protected function get_form() {
        $form = new ActivityForm();
        if ($this->_view === 'update')
            $form->delete_field('cover_id');
        return $form;
    }

    /** Maps a valid form to its database representation */
    protected function process_form_data($data) {
        if (array_key_exists('cover_id', $data))
            $data['cover_id'] = substr($data['cover_id'], 1);

        if (empty($data['deadline']))
            $data['deadline'] = null;

        if (!is_numeric($data['available_amount']))
            $data['available_amount'] = null;

        // Convert booleans to tinyints
        $data['active'] = empty($data['active']) ? 0 : 1;
        $data['needs_registration'] = empty($data['needs_registration']) ? 0 : 1;
        
        parent::process_form_data($data);   
    }

    /** Returns the default context */
    protected function get_default_context() {
        $context = parent::get_default_context();
        if ($this->_view === 'update') {
            $context['object'] = $this->get_object();
            $context['object'] = inject_activity_data($context['object']);
        }
        return $context;
    }


    /** Returns the name of the template to use */
    protected function get_template($view_name='') {
        if ($view_name === 'form' && $this->_view === 'update')
            $view_name = 'update_form';
        return parent::get_template($view_name);
    }
}

// Create and run subdomain view
$view = new ActivityAdminView('shop_admin', 'Activity Admin', get_model('Activity'));
$view->run();
