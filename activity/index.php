<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/ShopView.class.php';
require_once 'include/init.php';


/** Renders and processes CRUD operations for the Signup Model */
class ActivitiesView extends ShopView
{
    protected $template_base_name = 'templates/activity/activity';

    /** Runs the list_products view */
    protected function run_list() {
        $activities = $this->get_model('product')->get();
        usort(
            $activities,
            function ($a, $b) {
                return strtotime($a['details']->van) - strtotime($b['details']->van);
            }
        );
        $passe_partouts = get_model('PassePartout')->get();
        return $this->render_template($this->get_template(), [
            'activities' => $activities,
            'passe_partouts' => $passe_partouts,
            'user' => get_user()
        ]);
    }

    /** Runs the add view */
    protected function run_add() {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST')
            throw new HttpException(405, 'Method not allowed!');

        if (array_key_exists('activity', $_POST)) {
            $activities = $this->get_model('product')->get(['id__in' => $_POST['activity']]);
            foreach ($activities as $activity) {
                $res = array_filter(
                    $this->get_cart()->get_items(),
                    function ($item) use ($activity) {
                        return $item['id'] == $activity['id'];
                    }
                );
                if (!$res)
                    $this->get_cart()->add_item($activity);
            }
        }

        return $this->redirect('?view=purchase');
    }

    /** Returns the Cart object tot use for the view */
    protected function get_cart(){
        if (!isset($this->_cart))
            $this->_cart = new Cart('registration');
        return $this->_cart;
    }

    /** Returns the default context */
    protected function get_default_context() {
        $context = parent::get_default_context();

        if (!empty(get_user()) && !empty(get_user()['passe_partout_id'])) {
            $pp_activities = get_model('PassePartoutActivity')->get(['passe_partout_id' => get_user()['passe_partout_id']]);
            $context['passe_partout_activities'] = array_map(
                function ($value) { return $value['activity_id']; }, 
                $pp_activities
            );
        } else
            $context['passe_partout_activities'] = [];

        if ($this->_view === 'list' && !empty(get_user()))
            $context['registrations'] =  array_map(
                function ($val) {
                    return $val['activity_id'];
                },
                $this->get_model('purchase')->get(['user_id' => get_user()['id'], 'status' => 1])
            );
        else
            $context['registrations'] = [];

        return $context;
    }

    /** Returns the Model object to use for the view */
    protected function get_model($type='product') {
        if ($type === 'product')
            return get_model('Activity');
        elseif ($type === 'purchase')
            return get_model('Registration');
        elseif ($type === 'user')
            return get_model('User');
        else
            throw new RuntimeException('Unknown model requested!');
    }

    protected function get_product_form($product_id) {
        return null;
    }

    /** Returns the Form to use to obtain user information */
    protected function get_user_form() {
        $form = create_user_form();
        $form->add_fields([
            'remarks' => new TextAreaField ('Remarks', true),
            'accept_terms' => new CheckBoxField ('I have read and accepted the terms and conditions', false),
            'accept_costs' => new CheckBoxField ('I allow Cover to deduct the costs from my bank account through SEPA direct debit', false),
        ]);
        return $form;
    }

    /**
     * Processes the form data for the purchase view:
     * Creates a user, creates purchases and empties the cart.
     */
    protected function process_purchase($data) {
        $data = process_user_data($data);

        $user_data = $data;
        unset($user_data['accept_terms']);
        unset($user_data['accept_costs']);
        unset($user_data['remarks']);

        if (empty(get_user()))
            $user_id = $this->get_model('user')->create($user_data);
        else {
            $user_id = get_user()['id'];
            $this->get_model('user')->update_by_id($user_id, $user_data);
        }

        $user_registrations = array_map(
            function ($val) {
                return $val['activity_id'];
            },
            $this->get_model('purchase')->get(['user_id' => $user_id, 'status' => 1])
        );

        $activities = $this->get_model('product')->get();
        $errors = [];

        foreach ($this->get_cart()->get_items() as $id => $activity) {
            $act = array_filter(
                $activities, 
                function ($val) use ($activity) {
                    return $val['id'] == $activity['id']; 
                }
            );
            $act = array_shift($act);

            if (!in_array($user_data['type'], explode(';', $act['allowed_types']))) {
                $errors[] = sprintf('You are not allowed to register for %s as %s.',  $act['details']->kop, $this->get_model('user')::$type_options[$user_data['type']]);
                $this->get_cart()->delete_item($id);
            } else if (in_array($act['id'], $user_registrations)) {
                $errors[] = sprintf('You\'ve already registered for %s.', $act['details']->kop);
                $this->get_cart()->delete_item($id);
            } else if (deadline_has_lapsed($act)) {
                $errors[] = sprintf('The registration deadline for %s has lapsed.', $act['details']->kop);
                $this->get_cart()->delete_item($id);
            } else if (is_sold_out($act, 1, 'registrations')) {
                $errors[] = sprintf('There are no spots left for %s.', $act['details']->kop);
                $this->get_cart()->delete_item($id);
            } else {
                $this->get_model('purchase')->create([
                    'user_id' => $user_id,
                    'activity_id' => $activity['id'],
                    'accept_terms' => empty($data['accept_terms']) ? 0 : 1,
                    'accept_costs' => empty($data['accept_costs']) ? 0 : 1,
                    'remarks' => $data['remarks'],
                ]);
            }
        }

        $data['type'] = $this->get_model('user')::$type_options[$data['type']];
        $data['cart'] = $this->get_cart()->get_items();

        $this->get_cart()->empty();    

        $email = sprintf('%s <%s>', filter_var($data['name'], FILTER_SANITIZE_EMAIL), filter_var($data['email'], FILTER_SANITIZE_EMAIL));
        $success = send_mail(
            'no-reply@svcover.nl',
            $email,
            $this->render_template($this->get_template('email'), $data),
            null,
            [ sprintf('Reply-To: %s', ADMIN_EMAIL) ]
        );

        // Determine wether email has ben send succesfully
        if (!$success)
            $errors[] = 'Your registration was stored in our database, but we failed to send a confirmation email.';

        if (!empty($errors))
            throw new HttpException(500, implode("\n\n", $errors));
    }
}

// Create and run subdomain view
$view = new ActivitiesView('activity', 'Activities');
$view->run();
