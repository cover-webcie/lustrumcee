# LustrumCee

A website for Covers fifth lustrum, which takes place in 2018.


## Project

This projects main folder follows the following structure:

- `include` backend framework
  - `config.php` Project configuration
  - `...` backend framework files & classes
- `static` static frontend assets
  - `css`
    - `style.css` CSS overrides
    - `...` project specific css
  - `img`
    - `...` project specific images
  - `js`
    - `...` project specific javascript
- `templates` frontend templates
  - `layout.phtml` base template for all pages
  - `...` view specific templates
- `index.php` main view file
- `...` view files, eg. `committee.php`