<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/init.php';

/** Renders and processes CRUD operations for the Signup Model */
class RegistrationsView extends ModelView
{
    protected $views = ['delete', 'list'];
    protected $template_base_name = 'templates/passe_partout/purchases';

    /** 
     * Run the page, but only for logged in committee members. 
     * Non-admins are only allowed to see a list of their redirects
     */
    public function run_page() {
        if (!cover_session_in_committee(ADMIN_COMMITTEE))
            throw new HttpException(403, 'You need to be LustrumCee to see this page as admin!');

        return parent::run_page();
    }
}

// Create and run subdomain view
$view = new RegistrationsView('registrations', 'Registrations', get_model('PassePartoutPurchase'));
$view->run();
