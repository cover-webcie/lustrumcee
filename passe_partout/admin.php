<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/init.php';


class PassePartoutForm extends Bootstrap3Form
{
    public function __construct() {
        // Create fields
        $fields = [
            'name'               => new StringField   ('Name',                             false, ['maxlength' => 255, 'placeholder' => 'Gold']),
            'description'        => new TextAreaField ('Description',                      false, ['maxlength' => 1024]),
            'allowed_types'      => new StringField   ('Allowed participant types',        false, ['maxlength' => 255, 'placeholder' => implode(';', array_keys(get_model('User')::$type_options))]),
            'price'              => new NumberField   ('Price',                            false, ['min' => 0, 'max' => 9999.99, 'step' => 0.01, 'placeholder' => '50.00']),
            'available_amount'   => new NumberField   ('Available amount (optional)',      true,  ['min' => 0, 'placeholder' => 'leave blank for unlimited']),
            'active'             => new CheckBoxField ('Active',                           true)
        ];

        parent::__construct('passe_partout', $fields);
    }

    /** Implement custom validation */
    public function validate() {
        $result = parent::validate();

        if (!empty(array_diff(explode(';', $this->get_value('allowed_types')), array_keys(get_model('User')::$type_options)))) {
            $this->get_field('allowed_types')->errors[] = 'Allowed participant types must be one or more of the following options: ' . implode(';', array_keys(get_model('User')::$type_options));
            $result = false &&  $result;
        }

        return $result;
    }
}


/** Renders and processes CRUD operations for the ShopProduct Model */
class PassePartoutAdminView extends ModelView
{
    protected $views = ['create', 'update', 'list'];
    protected $template_base_name = 'templates/passe_partout/admin';

    /** 
     * Run the page, but only for logged in committee members. 
     * Non-admins are only allowed to see a list of their redirects
     */
    public function run_page() {
        if (!cover_session_logged_in())
            throw new HttpException(401, 'Unauthorized', sprintf('<a href="%s" class="btn btn-primary">Login and get started!</a>', cover_login_url()));
        else if (!cover_session_in_committee(ADMIN_COMMITTEE))
            throw new HttpException(403, 'You need to be LustrumCee to see this page!');

        if ($this->_view === 'activities')
            return $this->run_activities();

        return parent::run_page();
    }

    /** Maps a valid form to its database representation */
    protected function process_form_data($data) {
        if (!is_numeric($data['available_amount']))
            $data['available_amount'] = null;

        // Convert booleans to tinyints
        $data['active'] = empty($data['active']) ? 0 : 1;
        
        parent::process_form_data($data);   
    }

    /** Returns the default context */
    protected function get_default_context() {
        $context = parent::get_default_context();
        if ($this->_view === 'update') {
            $context['object'] =  $this->get_object();
            $context['object'] = $context['object'];
        }
        return $context;
    }
}

// Create and run subdomain view
$view = new PassePartoutAdminView('passe_partout_admin', 'Passe Partout Admin', get_model('PassePartout'), new PassePartoutForm());
$view->run();
