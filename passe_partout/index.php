<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/init.php';


/** Renders and processes CRUD operations for the ShopProduct Model */
class PassePartoutPurchaseView extends FormView
{
    protected $template_base_name = 'templates/passe_partout/passe_partout';

    protected $_object;

    /** Renders response indicating whether the valid form was successfully processed (or not) */
    protected function form_valid($form){
        try {
            $this->process_form_data($form->get_values());
            $context = ['status' =>  'success'];
        } catch (Exception $e) {
            $context = [
                'status' => 'error', 
                'message' => 'Something went wrong: ' . $e->getMessage()
            ];
        }
        return $this->render_template($this->get_template('form_processed'), $context);
    }

    protected function process_form_data($data) {
        $data = process_user_data($data);

        $passe_partout = $this->get_object();

        $user_data = $data;
        unset($user_data['accept_terms']);
        unset($user_data['accept_costs']);
        unset($user_data['remarks']);

        if (empty(get_user()))
            $user_id = get_model('User')->create($user_data);
        else {
            $user_id = get_user()['id'];
            get_model('User')->update_by_id($user_id, $user_data);
        }

        $errors = [];

        if (!in_array($user_data['type'], explode(';', $passe_partout['allowed_types'])))
            $errors[] = sprintf('You are not allowed to register for %s as %s.',  $passe_partout['name'], get_model('User')::$type_options[$user_data['type']]);
        else if (!empty(get_model('User')->get_by_id($user_id)['passe_partout_id']))
            $errors[] = sprintf('You\'ve already purchased a passe-partout');
        else if (is_sold_out($passe_partout, 1, 'registrations'))
            $errors[] = sprintf('There are no %s left.', $passe_partout['name']);
        else {
            get_model('PassePartoutPurchase')->create([
                'user_id' => $user_id,
                'passe_partout_id' => $passe_partout['id'],
                'accept_terms' => empty($data['accept_terms']) ? 0 : 1,
                'accept_costs' => empty($data['accept_costs']) ? 0 : 1,
                'remarks' => $data['remarks'],
            ]);

            $data['type'] = get_model('User')::$type_options[$data['type']];
            $data['passe_partout'] = $passe_partout; 

            $email = sprintf('%s <%s>', filter_var($data['name'], FILTER_SANITIZE_EMAIL), filter_var($data['email'], FILTER_SANITIZE_EMAIL));
            $success = send_mail(
                'no-reply@svcover.nl',
                $email,
                $this->render_template($this->get_template('email'), $data),
                null,
                [ sprintf('Reply-To: %s', ADMIN_EMAIL) ]
            );

            // Determine wether email has ben send succesfully
            if (!$success)
                $errors[] = 'Your purchase was stored in our database, but we failed to send a confirmation email.';
        }

        if (!empty($errors))
            throw new HttpException(500, implode("\n\n", $errors));
    }

    /** Returns the default context */
    protected function get_default_context() {
        $context = parent::get_default_context();
        $context['object'] =  $this->get_object();
        return $context;
    }

    protected function get_form() {
        $form = create_user_form();
        $form->add_fields([
            'remarks' => new TextAreaField ('Remarks', true),
            'accept_terms' => new CheckBoxField ('I have read and accepted the terms and conditions', false),
            'accept_costs' => new CheckBoxField ('I allow Cover to deduct the costs from my bank account through SEPA direct debit', false),
        ]);
        return $form;
    }

    protected function get_object() {
        if (!isset($_GET['id']))
            throw new HttpException(400, 'Please provide an ID!');

        if (!isset($this->_object)){
            $this->_object = get_model('PassePartout')->get_by_id($_GET['id']);

            if (empty($this->_object))
                throw new HttpException(404, 'No object found for id');
        }

        return $this->_object;
    }
}

// Create and run subdomain view
$view = new PassePartoutPurchaseView('passe_partout', 'Passe Partout');
$view->run();
