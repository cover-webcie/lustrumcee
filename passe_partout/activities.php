<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/init.php';

/** Renders and processes CRUD operations for the ShopProduct Model */
class PassePartoutActivitiesView extends ModelView
{
    protected $views = ['create', 'list'];
    protected $template_base_name = 'templates/passe_partout/activities';

    /** 
     * Run the page, but only for logged in committee members. 
     * Non-admins are only allowed to see a list of their redirects
     */
    public function run_page() {
        if ($this->_view !== 'list' && !cover_session_in_committee(ADMIN_COMMITTEE))
            throw new HttpException(403, 'You need to be LustrumCee to see this page!');

        return parent::run_page();
    }

    /** Runs the create view */
    protected function run_create() {
        $object = $this->get_object();

        $activities = get_model('Activity')->get();
        $activity_ids = array_map(
            function ($value) { return $value['id']; }, 
            $activities
        );

        $pp_activities = get_model('PassePartoutActivity')->get(['passe_partout_id' => $object['id']]);
        $pp_activities = array_map(
            function ($value) { return $value['activity_id']; }, 
            $pp_activities
        );

        $errors = [];
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            get_model('PassePartoutActivity')->delete(['passe_partout_id' => $object['id']]);
            foreach ($_POST['activity'] as $activity_id) {
                if (!in_array($activity_id, $activity_ids)) {
                    $errors[] = sprintf('%d is not a valid activity ID', $activity_id);
                    continue;
                }
                get_model('PassePartoutActivity')->create([
                    'passe_partout_id' => $object['id'],
                    'activity_id' => $activity_id
                ]);
            }
            $this->redirect('admin.php');
        }

        return $this->render_template($this->get_template('form'), compact('object', 'activities', 'pp_activities', 'errors'));
    }

    /** Runs the list view */
    protected function run_list() {
        $object = $this->get_object();
        $activities = get_model('PassePartoutActivity')->get(['passe_partout_id' => $object['id']]);
        $activities = inject_activity_data($activities, 'activity_cover_id');
        return $this->render_template($this->get_template(), compact('object', 'activities'));
    }
}

// Create and run subdomain view
$view = new PassePartoutActivitiesView('passe_partout_activities', 'Passe Partout Activities', get_model('PassePartout'));
$view->run();
